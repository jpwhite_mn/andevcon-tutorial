# README #

This repository contains the slides, labs, and other materials for **Android UI Testing with Espresso Tutorial** held 12/1/15 and presented by **Jim White** @ 2015 AnDevCon, Santa Clara, CA

### Repository Contents ###

* Tutorial Slides (Android UI Testing with Espresso.pptx)
* Lab documents (Lab1-3.docx files)
* Android Studio lab projects (3 labs with starter, solution and bonus code projects in Labs folder)
* Android Studio Setup document (Android Studio Setup.docx)

### Attending this tutorial?   ###
Students attending Android UI Testing with Espresso with Jim White should install the latest Android Studio and then update Android Studio with Android Support Repository (v15 or better).  A document to guide you through the installation and the update process is located in this repository (see the Android Studio Setup.docx)

Students should also use git to download the Android lab projects located in this repository.  See Bit Bucket documentation (https://confluence.atlassian.com/display/BITBUCKET/Bitbucket+101) for help with Git or Bit Bucket.

##Due to the potentially over taxed conference Wifi, don’t wait until class to download and install these files!##

© Copyright 2015, James P White,  All Rights Reserved.